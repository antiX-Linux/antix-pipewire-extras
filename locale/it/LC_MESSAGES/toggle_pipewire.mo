��          L      |       �      �   |   �   t   /  "   �     �  �  �     e  �   n  �     %   �     �                                         PipeWire PipeWire audio server IS NOT currently on the antiX startup and WILL NOT start automatically the next time you restart antiX PipeWire audio server IS currently on the antiX startup and WILL start automatically the next time you restart antiX PipeWire seems to not be installed Reboot Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-08-04 13:02+0000
Last-Translator: Davide Pedrotti, 2023
Language-Team: Italian (https://app.transifex.com/anticapitalista/teams/10162/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 PipeWire Il server audio PipeWire attualmente NON È nelle applicazioni di avvio di antiX e NON SI AVVIERÀ automaticamente la prossima volta che avvierai antiX Il server audio PipeWire attualmente È nelle applicazioni di avvio di antiX e si AVVIERÀ automaticamente la prossima volta che avvierai antiX PipeWire sembra non essere installato Riavvia 