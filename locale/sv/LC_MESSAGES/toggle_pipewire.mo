��          L      |       �      �   |   �   t   /  "   �     �  �  �     S  �   \  o   �  0   T  	   �                                         PipeWire PipeWire audio server IS NOT currently on the antiX startup and WILL NOT start automatically the next time you restart antiX PipeWire audio server IS currently on the antiX startup and WILL start automatically the next time you restart antiX PipeWire seems to not be installed Reboot Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-08-04 13:02+0000
Last-Translator: Henry Oquist <henryoquist@nomalm.se>, 2023
Language-Team: Swedish (https://app.transifex.com/anticapitalista/teams/10162/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
 PipeWire PipeWire ljudserver ÄR för närvarande inte på antiX startup och KOMMER INTE att starta automatiskt nästa gång du startar om antiX PipeWire ljudserver ÄR nu på antiX startup och KOMMER att starta automatiskt nästa gång du startar om antiX Det verkar som att PipeWire inte är installerat Starta om 