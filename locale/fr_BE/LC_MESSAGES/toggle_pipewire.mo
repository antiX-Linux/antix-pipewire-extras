��          L      |       �      �   |   �   t   /  "   �     �  �  �     ~  �   �  �   /  &   �     �                                         PipeWire PipeWire audio server IS NOT currently on the antiX startup and WILL NOT start automatically the next time you restart antiX PipeWire audio server IS currently on the antiX startup and WILL start automatically the next time you restart antiX PipeWire seems to not be installed Reboot Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-08-04 13:02+0000
Last-Translator: Wallon Wallon, 2023
Language-Team: French (Belgium) (https://app.transifex.com/anticapitalista/teams/10162/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 PipeWire Le serveur audio PipeWire n’est pas présent dans la procédure de démarrage d’antiX et ne se lancera pas automatiquement lors du prochain redémarrage d’antiX. Le serveur audio PipeWire est présent dans la procédure de démarrage d’antiX et se lancera automatiquement lors du prochain redémarrage d’antiX. PipeWire semble ne pas être installé Redémarrer 