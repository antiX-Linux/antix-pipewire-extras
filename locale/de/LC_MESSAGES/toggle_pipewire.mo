��          L      |       �      �   |   �   t   /  "   �     �  b  �     1  �   :  �   �  *   R  
   }                                         PipeWire PipeWire audio server IS NOT currently on the antiX startup and WILL NOT start automatically the next time you restart antiX PipeWire audio server IS currently on the antiX startup and WILL start automatically the next time you restart antiX PipeWire seems to not be installed Reboot Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-08-04 13:02+0000
Last-Translator: aer, 2023
Language-Team: German (https://app.transifex.com/anticapitalista/teams/10162/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
 PipeWire Der PipeWire-Audio-Server befindet sich derzeit NICHT im antiX-Systemstart und wird beim nächsten Neustart von antiX NICHT automatisch gestartet Der PipeWire-Audio-Server befindet sich derzeit im antiX-Systemstart und wird beim nächsten Neustart von antiX automatisch gestartet PipeWire ist anscheinend nicht installiert Neustarten 