��          L      |       �      �   |   �   t   /  "   �     �  �  �     �  �   �  �   {  <   '     d                                         PipeWire PipeWire audio server IS NOT currently on the antiX startup and WILL NOT start automatically the next time you restart antiX PipeWire audio server IS currently on the antiX startup and WILL start automatically the next time you restart antiX PipeWire seems to not be installed Reboot Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-08-04 13:02+0000
Last-Translator: Caarmi, 2023
Language-Team: Russian (https://app.transifex.com/anticapitalista/teams/10162/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 PipeWire Аудио-сервер PipeWire ОТКЛЮЧЕН при запуске antiX и НЕ будет запущен автоматически после перезапуска antiX Аудио-сервер PipeWire ВКЛЮЧЕН при запуске antiX и будет запущен автоматически после перезапуска antiX PipeWire, скорее всего, не установлен Перезагрузка 