��          L      |       �      �   |   �   t   /  "   �     �  n  �     =  �   F  �   �  &   �     �                                         PipeWire PipeWire audio server IS NOT currently on the antiX startup and WILL NOT start automatically the next time you restart antiX PipeWire audio server IS currently on the antiX startup and WILL start automatically the next time you restart antiX PipeWire seems to not be installed Reboot Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-08-04 13:02+0000
Last-Translator: Risto Pärkkä, 2023
Language-Team: Finnish (https://app.transifex.com/anticapitalista/teams/10162/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
 PipeWire PipeWire-äänipalvelin EI OLE tällä hetkellä antiX-käynnistyksessä, eikä se käynnisty automaattisesti, kun seuraavan kerran käynnistät antiX:n uudelleen PipeWire-äänipalvelin ON tällä hetkellä antiX-käynnistysvaiheessa ja se käynnistyy automaattisesti, kun seuraavan kerran käynnistät antiX:n uudelleen PipeWire ei näytä olevan asennettuna Käynnistä uudelleen 