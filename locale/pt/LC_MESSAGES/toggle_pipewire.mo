��          L      |       �      �   |   �   t   /  "   �     �  �  �     �  �   �  �   *  *   �  	   �                                         PipeWire PipeWire audio server IS NOT currently on the antiX startup and WILL NOT start automatically the next time you restart antiX PipeWire audio server IS currently on the antiX startup and WILL start automatically the next time you restart antiX PipeWire seems to not be installed Reboot Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-08-04 13:02+0000
Last-Translator: Hugo Carvalho <hugokarvalho@hotmail.com>, 2023
Language-Team: Portuguese (https://app.transifex.com/anticapitalista/teams/10162/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 PipeWire O servidor de áudio PipeWire NÃO ESTÁ configurado no arranque do antiX e NÃO irá iniciar automaticamente na próxima vez que reiniciar o antiX O servidor de áudio PipeWire ESTÁ configurado no arranque do antiX e IRÁ iniciar automaticamente na próxima vez que reiniciar o antiX Parece que o PipeWire não está instalado Reiniciar 